
import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Injectable } from '@angular/core';
import { Consulta } from '../_model/consulta';

@Injectable({
  providedIn: 'root'
})
export class ConsultaService {

  url: string = `${HOST}/api`;

  constructor(private http: HttpClient) { }
 
  buscar(filtroConsulta: String) {
    return this.http.get<Consulta[]>(`${this.url}/agencia/${filtroConsulta}`, );
  }
   
}