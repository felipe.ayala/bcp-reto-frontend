import { DialogoDetalleComponent } from './dialogo-detalle/dialogo-detalle.component';
import { ConsultaService } from './../../_service/consulta.service';
import { Consulta } from './../../_model/consulta';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  form: FormGroup;
  displayedColumns = ['agencia','departamento', 'provincia', 'distrito', 'direccion', /* 'lat', 'lon',*/'acciones'];
  dataSource: MatTableDataSource<Consulta>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  maxFecha: Date = new Date();

  constructor(private consultaService: ConsultaService, public dialog: MatDialog) {   
  }

  ngOnInit() {
    this.form = new FormGroup({
      'agencia': new FormControl('')
    });
  }

  buscar() {
    let filtro = this.form.value['agencia'];
    filtro = filtro.toLocaleLowerCase();
 

      this.consultaService.buscar(filtro).subscribe(data => {
        this.dataSource = new MatTableDataSource(data)
      });

  }

  verDetalle(consulta: Consulta) { 
    this.dialog.open(DialogoDetalleComponent, {      
      data: consulta
    });
  }

}
