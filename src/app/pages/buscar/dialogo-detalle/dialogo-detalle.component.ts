
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Consulta } from './../../../_model/consulta';
import { Component, OnInit, Inject } from '@angular/core';
import { ZOOM, TIME_UPDATE_GEOLOCALIZATION } from '../../../_shared/var.constant';
import { MiMarker } from './../../../_model/MiMarker';

@Component({
  selector: 'app-dialogo-detalle',
  templateUrl: './dialogo-detalle.component.html',
  styleUrls: ['./dialogo-detalle.component.css']
})
export class DialogoDetalleComponent implements OnInit {
  zoom = ZOOM;
  apiGeoLocalization = navigator.geolocation;
  consulta: Consulta;
  miMarker: MiMarker;
  constructor(public dialogRef: MatDialogRef<DialogoDetalleComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Consulta ) { 
      this.miMarker = new MiMarker();
    }

  ngOnInit() {
    this.consulta = this.data; 
    this.cargarDatos();
    //console.log(this.consulta);
  }
  
  cargarDatos() {
    this.apiGeoLocalization.getCurrentPosition(position => {
      console.log(this.consulta);
      this.miMarker.latitud = Number(this.consulta.lon);
      this.miMarker.longitud = Number(this.consulta.lat);
      this.miMarker.agencia = this.consulta.agencia; 
      console.log(this.miMarker);
    }, err => {
      console.log(err);
    }, { timeout: TIME_UPDATE_GEOLOCALIZATION });
  }

  cancelar() {
    this.dialogRef.close();
  }

}
